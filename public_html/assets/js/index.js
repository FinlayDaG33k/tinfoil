/**
 * Define core variables here
 */
var selected;
var topicData;
var bingoChart = [];
var topics = [
  'vaccination'
];

/**
 * Document finished loading
 */
$(() => {
  // Add topics to the select box
  $.each(topics, (index, topic) => {
    $("#select-topic").append(new Option(
      topic, topic, true, true
    ));
  });

  // Show the modal
  $("#launch-modal").addClass("is-active");

  // Start the bingo
  $("#submit-topic").on('click', () => {
    // Set the selected topic
    selected = $("#select-topic").val();

    // Download the dataset
    $.get(`/topics/${selected}.json`).done((data) => {
      topicData = data;
    })

    // Get 24 unique random values from the dataset
    .done(() => {
      var limit = topicData.length - 1;
      var amount = 24;
      var lowerBound = 0;
      var upperBound = topicData.length - 1;

      // Make sure we can't try to get more values than we bargained for
      if(limit < amount) amount = limit;

      // Get our unique values
      while (bingoChart.length < amount) {
        var randomNumber = Math.floor(Math.random()*(upperBound - lowerBound) + lowerBound);
        if (bingoChart.indexOf(topicData[randomNumber]) == -1) {
          topicData[randomNumber].done = false;
          bingoChart.push(topicData[randomNumber]);
        }
      }
    })
    // Update our grid
    .done(() => {
      var source = document.getElementById("entry-template").innerHTML;
      var template = Handlebars.compile(source);

      bingoChart.forEach(item => {
        var context;

        // Check if we need to add a freebie
        if(bingoChart.indexOf(item) != 0 && bingoChart.indexOf(item) % 12 == 0) {
          context = {
            text: "freebie",
            freebie: true
          };
          var html = template(context);
          $("#chart").append(html);
        }

        switch(item.type){
          case "quote":
            item.content = `"${item.content}"`;
            break;
          case "action":
            item.content = `*${item.content}*`;
            break;
        }

        context = {
          text: item.content,
          submitter: item.submitter || "Anonymous",
          freebie: false
        };
        
        var html = template(context);
        $("#chart").append(html);
      });
    })
    // Hide the modal
    .done(() => {
      $("#launch-modal").removeClass("is-active");
    });
  });

  $(document).on('click', `button[data-action="hit"]`, (e) => {
    let self = e.currentTarget;
    $(self).removeClass('is-danger').addClass('is-success');
  })
});